clear all; clc

load generate_data_si1
clear x t

ib = 15; b = bs(ib); 
itr = randi(ntr);

itr = 824; % good for cm 

v = 0.1;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

decision = @(x) 0 + 1*(norm(x-x1)<v/2) + 2*(norm(x-x2)<v/2); 

desc{1} =  '{\bf Model 1}: Action initiation after decision';
fixate{1} = @(z,b,x,y) x1*(z<b(1) && all(y~=x2) ) + y*( ~(z<b(1) && all(y~=x2) ) && ~(z>b(2) && all(y~=x1)) ) + x2*(z>b(2) && all(y~=x1));

desc{2} =  '{\bf Model 2}: Action initiation and changes of mind';
fixate{2} = @(z,b,x,y) x1*(z<b(1)) + y*(z>=b(1) && z<=b(2)) + x2*(z>b(2));

desc{3} = '{\bf Model 3}: Continual action preparation';
% fixate{3} = @(z,b,x,y) x1*(z<b(1)) + [w*(z-b(1)/2-b(2)/2)/(b(2)-b(1)) ht]*(z>=b(1) & z<=b(2)) + x2*(z>b(2));
fixate{3} = @(z,b,x,y) x1*(z<b(1)) + (x2*abs(b(1)-z)/abs(2*b(1)) + x1*abs(b(2)-z)/abs(2*b(2)))*(z>=b(1) & z<=b(2)) + x2*(z>b(2));

desc{4} = '{\bf Model 4}: Action preparation and commitment';
fixate{4} = @(z,b,x,y) fixate{3}(z+4*b(2)*(norm(x-x1)-norm(x-x2))/(norm(x-x1)+norm(x-x2)),b,x,y);

rmod = 1:4;
for imod = rmod

  t = 0; 
  x = [0 0]; y = x(1,:);
  
  dec = 0;
  while dec==0; t = t + 1;
    
    y(t+1,:) = fixate{imod}(z(t,itr),[-b b],x(t,:),y(t,:));
    
    dx = y(t+1,:) - x(t,:);
    if norm(dx)>0; dx = v*dx/norm(dx); end
    x(t+1,:) = x(t,:) + dx;
    
    dec = decision(x(t,:));
    if t==nt-1; dec = nan; end
    
  end

  res{imod}.t = t;
  res{imod}.x = x;
  res{imod}.y = y;
  
end

%% figures for paper

figure(3); clf; hold on; box on

x = res{3}.x;
t = res{3}.t;
iv = [7 13 17];

fill([-0.15-w/2 -0.15-w/2 w/2+0.15 w/2+0.15],[-0.15 ht+0.15 ht+0.15 -0.15],0.9*[1 1 1])
plot(-w/2,ht,'.k', w/2,ht,'.k','markersize',30)
axis([-0.15-w/2 0.15+w/2 -0.15 ht+0.15])
htit = title({'{\bf Model}:','Move towards focus'},'fontsize',15);
pos = get(htit,'Position'); set(htit,'Position',[pos(1) pos(2)-0.0 pos(3)])
set(gca,'xticklabel','','yticklabel','')

xlabel('Horizontal position, x','fontsize',15); 
ylabel('Vertical position, y','fontsize',15);
text(w/10,ht*1.05,{' focus'},'horizontalalignment','center','fontsize',15); 
text(-w/2,ht*0.925,{'T1'},'horizontalalignment','center','fontsize',15); 
text(w/2,ht*0.925,{'T2'},'horizontalalignment','center','fontsize',15); 

plot(x(1:t,1), x(1:t,2), '.-b','markersize',7,'linewidth',1)

for i = iv
  v = x(i,:); while v(end,2)<ht; v(end+1,:) = v(end,:) + ( x(i+1,:) - x(i,:) )/10; end 
  plot(v(:,1),v(:,2),':k'); plot(v(end,1),v(end,2),'kx','markersize',10,'linewidth',2)
end

set(gca,'position',[0.1 0.15 0.65 0.7],'units','normalized')

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 4.5 4])
eval(['export_fig ' mfilename '.png -r300 -painters']);
