run generate_data_si1 and generate_data_si4 scripts
run model_si1 and model_si4 scripts (used in Fig 5 and Fig 6)
then run fig2 to fig7 to simulate and generate figures 

there is also a demo for real-time trajectory generation (for talks)

note that figures will look different from paper because trajectories are randomly generated

data for experimental_analysis not supplied (too large) but output .mat file is supplied
data available from authors of that study (details in paper)

Code supplied unsupported

N. Lepora and G. Pezzulo 2015
 