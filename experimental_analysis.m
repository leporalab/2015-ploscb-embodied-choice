clear all
load('data/data')

[ntr,nt0] = size(data);
nt = nt0/2; rt = 1:nt;

for itr = 1:ntr
  xdata{itr}(:,2) = -data(itr,rt);
  xdata{itr}(:,1) = data(itr,nt+rt);
end

% average trajectories
ys = linspace(0,1.5,1001); nys = length(ys);
ntr = 150;

for itr = 1:ntr
  xdata{itr} = xdata{itr} + 0.0001*randn(size(xdata{itr}));
  x1(:,itr) = interp1(xdata{itr}(:,2),xdata{itr}(:,1),ys);
end

xav(:,1) = nanmean(x1');
xav(:,2) = ys;

rtr = randi(150,1,10);
rtr = [110   125   107    24   147    84    71    60    13    42];

w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
desc = {'','{\bf Experiment}:','Mouse-tracking data'};

figure(1); clf; hold on; box on;
plot(0,0,'r',0,0,'b')
fill([-0.15-w/2 -0.15-w/2 w/2+0.15 w/2+0.15],[-0.15 ht+0.15 ht+0.15 -0.15],0.9*[1 1 1])
for itr = rtr; plot(xdata{itr}(:,2),xdata{itr}(:,1)); end
plot(xav(:,2),xav(:,1),'r'); 
axis([-0.15-w/2 0.15+w/2 -0.15 ht+0.15])
title(desc,'fontsize',5);
set(gca,'xticklabel','','yticklabel','')

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 1 1])
eval(['export_fig ' mfilename '.jpg -r600 -painters']);


save(mfilename)


