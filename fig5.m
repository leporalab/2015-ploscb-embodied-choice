clear all; clc
t0 = [1.5 1.5 1.05];
rmod = 1:3;

v = 0.2;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

res1{1}.desc = {'{\bf Model 1}:','Action initiation','after decision'};
res1{2}.desc = {'{\bf Model 2}:','Action initiation','& changes of mind'};
res1{3}.desc = {'{\bf Model 3}:','Action preparation',' '};
res1{4}.desc = {'{\bf Model 4}:','Action preparation','& commitment'};

figure(5); clf
xpos = linspace(0.1,0.7,4);

subplot(1,4,1); hold on; box on;
plot(0,0,'r',0,0,'b')
fill([-0.15-w/2 -0.15-w/2 w/2+0.15 w/2+0.15],[-0.15 ht+0.15 ht+0.15 -0.15],0.9*[1 1 1])
plot(-w/2,ht,'.k', w/2,ht,'.k','markersize',15)
plot(-[0 1],[0 1.5],'r','linewidth',1.5);
plot(-[0 1],[0 1.5],'b','linewidth',1);
axis([-0.15-w/2 0.15+w/2 -0.15 ht+0.15])
htit = title(res1{1}.desc,'fontsize',10);
pos = get(htit,'Position'); set(htit,'Position',[pos(1) pos(2)-0.05 pos(3)])
set(gca,'xticklabel','','yticklabel','')

col = 'br'; leg = {'individual','trajectories','average','trajectory'};
for i = 1:2; plot([-1.05 -0.8]+0.95,0.5+(1-0.4*i)*[1 1],col(i)); end
for i = 1:4; text(-0.8+1-0.05*~rem(i,2),0.3+(1-0.2*i),leg{i},'fontsize',10); end

text(-0.05,1.245,'A','Units','Normalized','Fontsize',12,'Fontweight','bold')

load('model_si1.mat')
rtr_i{1} = 1*ntr/2 + (1:10);
rtr_i{2} = [ 34    48    15    27    28    33    33    97    70    28 ];
rtr_i{3} = [ 34    48    15    27    28    33    33    97    70    28 ];

set(gca,'position',[xpos(1) 0.1 0.17 0.7])

for imod1 = rmod(1:3)
  subplot(1,4,imod1+1); hold on; box on;
  
  fill([-0.15-w/2 -0.15-w/2 w/2+0.15 w/2+0.15],[-0.15 ht+0.15 ht+0.15 -0.15],0.9*[1 1 1])  
  [~, ib1] = min((res{imod1+2}.mt_dec/10-t0(imod1)).^2);
  rtr = rtr_i{imod1};

  plot(-w/2,ht,'.k', w/2,ht,'.k','markersize',15)

  for itr = rtr
    xdec{ib1,itr,imod1}(xdec{ib1,itr,imod1}(:,2)>1.5 , 2) = 1.5;
    plot(xdec{ib1,itr,imod1}(:,1),xdec{ib1,itr,imod1}(:,2),'linewidth',0.75)
  end
  plot(xav{ib1,imod1}(:,1),xav{ib1,imod1}(:,2),'r','linewidth',1.5)

  axis([-0.15-w/2 0.15+w/2 -0.15 ht+0.15])
  htit = title(res1{imod1+1}.desc,'fontsize',10);
  pos = get(htit,'Position'); set(htit,'Position',[pos(1)-0.05 pos(2)-0.05 pos(3)])

  set(gca,'xticklabel','','yticklabel','')
  
  % calculate gradients
  gradav1 =  mean(diff(-xav{ib1,imod1}(1:2,2))./diff(xav{ib1,imod1}(1:2,1)))
  gradav2 =  mean(diff(-xav{ib1,imod1}(end-1:end,2))./diff(xav{ib1,imod1}(end-1:end,1)))

  set(gca,'position',[xpos(imod1+1) 0.1 0.17 0.7])
  
  let = {'A','B','C','D','E'};
  text(-0.05,1.245,let{1+imod1},'Units','Normalized','Fontsize',12,'Fontweight','bold')

end

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 9.5 2.4])
eval(['export_fig ' mfilename '.png -r300 -painters']);
