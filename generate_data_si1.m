clear all; clc

nt = 500;    % max # time samples
si = 1;      % standard dev of noise
ntr = 1000;

bs = linspace(0.01,20,101); nbs = length(bs);

si = si*[1 1];
mu = [-1 1]/3;
g = @(s,mu,si) exp(-(s-mu).^2/(2*si^2))/sqrt(2*pi*si^2);

for itr = 1:ntr
  if ~rem(itr,10); disp(num2str(itr)); end
  
  tar(itr) = 1;
  s = mu(tar(itr)) + si(tar(itr))*randn(nt,1);
  z(:,itr) = cumsum(s);
end

for itr = 1:ntr
  for ib = 1:nbs
    t_1 = find( z(:,itr) >= bs(ib), 1, 'first'); if isempty(t_1); t_1 = nan; end
    t_2 = find( z(:,itr) <= -bs(ib), 1, 'first'); if isempty(t_2); t_2 = nan; end  
    [t n] = min( [ t_1 t_2] ); if t==nt+2; t = nan; n = nan; end
    
    t_dec(ib,itr) = t;
    n_dec(ib,itr) = n;
  end
end

for ib = 1:nbs
  mt_dec(ib) = nanmean(t_dec(ib,:));
  me_dec(ib) = 1-nanmean(abs(n_dec(ib,:)-tar));
end

als = linspace(0.001,0.010,11); nal = length(als);
for ial = 1:nal
  for ib = 1:nbs
    r_dec(ib,ial) = (me_dec(ib)-min(me_dec)) + als(ial)*(mt_dec(ib)-min(mt_dec));
  end
  %   r_dec(:,ial) = r_dec(:,ial) - min(r_dec(:,ial));
end

figure(1); clf
subplot(2,2,1); plot(bs,mt_dec,'.-')
subplot(2,2,2); plot(bs,me_dec,'.-')
subplot(2,2,3); plot(bs,r_dec)

save(mfilename)

