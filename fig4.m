clear all; clc

load generate_data_si1
clear x t

ib = 15; b = bs(ib); 
itr = randi(ntr);

itr = 824; % good for cm 

v = 0.1;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

decision = @(x) 0 + 1*(norm(x-x1)<v/2) + 2*(norm(x-x2)<v/2); 

desc{1} =  '{\bf Model 1}: Action initiation after decision';
fixate{1} = @(z,b,x,y) x1*(z<b(1) && all(y~=x2) ) + y*( ~(z<b(1) && all(y~=x2) ) && ~(z>b(2) && all(y~=x1)) ) + x2*(z>b(2) && all(y~=x1));

desc{2} =  '{\bf Model 2}: Action initiation & changes of mind    ';
fixate{2} = @(z,b,x,y) x1*(z<b(1)) + y*(z>=b(1) && z<=b(2)) + x2*(z>b(2));

desc{3} = '{\bf Model 3}: Continual action preparation';
% fixate{3} = @(z,b,x,y) x1*(z<b(1)) + [w*(z-b(1)/2-b(2)/2)/(b(2)-b(1)) ht]*(z>=b(1) & z<=b(2)) + x2*(z>b(2));
fixate{3} = @(z,b,x,y) x1*(z<b(1)) + (x2*abs(b(1)-z)/abs(2*b(1)) + x1*abs(b(2)-z)/abs(2*b(2)))*(z>=b(1) & z<=b(2)) + x2*(z>b(2));

desc{4} = '{\bf Model 4}: Action preparation & commitment    ';
fixate{4} = @(z,b,x,y) fixate{3}(z+4*b(2)*(norm(x-x1)-norm(x-x2))/(norm(x-x1)+norm(x-x2)),b,x,y);

rmod = 1:4;
for imod = rmod

  t = 0; 
  x= [0 0]; y = x(1,:);
  
  dec = 0;
  while dec==0; t = t + 1;
    
    y(t+1,:) = fixate{imod}(z(t,itr),[-b b],x(t,:),y(t,:));
    
    dx = y(t+1,:) - x(t,:);
    if norm(dx)>0; dx = v*dx/norm(dx); end
    x(t+1,:) = x(t,:) + dx;
    
    dec = decision(x(t,:));
    if t==nt-1; dec = nan; end
    
  end

  res{imod}.t = t;
  res{imod}.x = x;
  res{imod}.y = y;
  
end

%% figures for paper

t1 = find(z(1:nt,itr)>b,1,'first'); if isempty(t1); t1 = nan; end
t2 = find(z(1:nt,itr)<-b,1,'first'); if isempty(t2); t2 = nan; end

figure(4); clf; 
subplot(4,2,[3 5]); hold on; box on
plot((0:nt)/20,[0; -z(1:nt,itr)],'.-k')
plot([0 nt],b*[1 1],':k', [0 nt],-b*[1 1],':k')
plot(t1*[1 1]/20,[-5.5 10],':r')
text(t1/20,-7.25,{'initial',' decision'},'horizontalalignment','center','fontsize',12)
plot(t2*[1 1]/20,[-5.5 10],':g')
text(t2/20,-7.25,{'change','of mind'},'horizontalalignment','center','fontsize',12)
axis([0 2 9*[-1 1]])
set(gca,'xtick',0:0.5:5,'ytick',-10:5:10,'fontsize',12)
ylabel({'accumulated information, z   '},'fontsize',12)
xlabel('time, t (sec)','fontsize',12)
title('Decision process','fontsize',15)
% set(gca,'position',[0.15 0.525 0.8 0.4],'units','normalized')
text(1.175,b+1.10,'upper bound, +b','fontsize',12)
text(1.175,-b-1.05,'lower bound, -b','fontsize',12)
text(-0.153,1.15,'A','Units','Normalized','Fontsize',18,'Fontweight','bold')

col = 'kbgr'; let = {'B','C','D','E'};
for imod = rmod
  subplot(4,2,imod*2); hold on; box on
  yp = res{imod}.y(:,1); yp(~yp) = nan; 
  if imod>2; yp(find(isnan(yp),1,'last')) = 0; end
  plot((0:length(yp)-1)/20,-yp,'.-k')
  plot(t1*[1 1]/20,[-10 10],':r')
  plot(t2*[1 1]/20,[-10 10],':g')
  axis([0 2 -1.5 1.5])
  set(gca,'ytick',[-1 1],'yticklabel',{'T2','T1'},'fontsize',12)
  title(desc{imod})
  if imod==4; xlabel('time, t (sec)','fontsize',12); end
  ylabel('focus')
  text(-0.225,1.5,let{imod},'Units','Normalized','Fontsize',18,'Fontweight','bold')
  
end

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 9 5.5])
eval(['export_fig ' mfilename '.png -r300 -painters']);
