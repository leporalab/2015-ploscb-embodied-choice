clear all; clc
load generate_data_si1

v = 0.2;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

ntr = 100; % change to 10000 for best states; to 100 for quick runs

decision = @(x) 0 + 1*(norm(x-x1)<v/2) + 2*(norm(x-x2)<v/2);

res{1}.desc = '{\bf baseline model}: DDM without action';
res{1}.mt_dec = mt_dec; res{1}.me_dec = me_dec; res{1}.nfail = sum(isnan(n_dec'));

res{2}.desc = {'{\bf model 1}:','action initiation after decision'};
res{2}.mt_dec = mt_dec + min_rt; res{2}.me_dec = me_dec; res{2}.nfail = sum(isnan(n_dec(:)));

res{3}.desc = {'','{\bf model 2}:','action initiation & changes of mind'};
fixate{1} = @(z,b,x,y) x1*(z<b(1)) + x*(z>=b(1) && z<=b(2)) + x2*(z>b(2));
% fixate{1} = @(z,b,x,y) x1*(z<b(1)) + y*(z>=b(1) && z<=b(2)) + x2*(z>b(2));

res{4}.desc = {'','{\bf model 3}:','action preparation'};
% fixate{2} = @(z,b,x,y) x1*(z<b(1)) + [w*(z-b(1)/2-b(2)/2)/(b(2)-b(1)) ht]*(z>=b(1) & z<=b(2)) + x2*(z>b(2));
fixate{2} = @(z,b,x,y) x1*(z<b(1)) + (x2*abs(b(1)-z)/abs(2*b(1)) + x1*abs(b(2)-z)/abs(2*b(2)))*(z>=b(1) & z<=b(2)) + x2*(z>b(2));

res{5}.desc = {'','{\bf model 4}:','action preparation & commitment'};
fixate{3} = @(z,b,x,y) fixate{2}(z+4*b(2)*(norm(x-x1)-norm(x-x2))/(norm(x-x1)+norm(x-x2)),b,x,y);

rmod = 1:3;

for imod = rmod
  
  for itr = 1:ntr
    if ~rem(itr,10); disp(num2str(itr)); end
    
    for ib = 1:nbs
      b = bs(ib);
      
      t = 0;
      clear x
      x(1,:) = [0 0]; y(1,:) = x(1,:);
      
      dec = 0;
      while dec==0; t = t + 1;
        
        y(t+1,:) = fixate{imod}(z(t,itr),[-b b],x(t,:),y(t,:));
        
        dx = y(t+1,:) - x(t,:);
        if norm(dx)>0; dx = v*dx/norm(dx); end
        x(t+1,:) = x(t,:) + dx;
        
        dec = decision(x(t,:));
        if t==nt-1; dec = nan; end
        
      end
      
      tdec(ib,itr,imod) = t;
      ndec(ib,itr,imod) = dec;
      xdec{ib,itr,imod} = x;
      
    end
  end
  
  for ib = 1:nbs
    res{imod+2}.mt_dec(ib) = nanmean(tdec(ib,:,imod));
    res{imod+2}.me_dec(ib) = nanmean(abs(ndec(ib,:,imod)-tar(1:ntr)));
    res{imod+2}.nfail(ib) = sum(isnan(ndec(ib,:,imod)));
  end
  
end

% save(mfilename)
% load(mfilename)

% remap trajectories terminating on the left to the righe
for imod = rmod
  for ib = 1:nbs
    for itr = 1:ntr
      
      if ndec(ib,itr,imod)==2
        xdec{ib,itr,imod}(:,1) = -xdec{ib,itr,imod}(:,1);
        xdec{ib,itr,imod}(:,2) = xdec{ib,itr,imod}(:,2);
      end
      
    end
  end
end

% average trajectories
ys = linspace(0,1.5,1001); nys = length(ys);
for imod = rmod
  for ib = 1:nbs
    clear x1
    
    for itr = 1:ntr
      xdec{ib,itr,imod} = xdec{ib,itr,imod} + 0.0001*randn(size(xdec{ib,itr,imod}));
      x1(:,itr) = interp1(xdec{ib,itr,imod}(:,2),xdec{ib,itr,imod}(:,1),ys);
    end
    
    xav{ib,imod}(:,1) = nanmean(x1');
    xav{ib,imod}(:,2) = ys;
    
  end
end

% figure(1); clf
% for imod = rmod
%   [~, ib] = min((res{imod+2}.mt_dec/10-2).^2);
%   subplot(1,length(rmod),imod); hold on; box on;
%   for itr = 1:ntr
%     plot(xdec{ib,itr,imod}(:,1),xdec{ib,itr,imod}(:,2),'linewidth',0.5)
%   end
%   plot(xav{ib,imod}(:,1),xav{ib,imod}(:,2),'r','linewidth',1)
%   axis([-1.25 1.25 -0.05 1.55])
%   title(res{imod+2}.desc,'fontsize',8);
%   text(0.6,0.5,['b=' num2str(bs(ib),'%2.0f')],'fontsize',6)
%   text(0.6,0.3,['e=' num2str(res{imod+2}.me_dec(ib),'%5.2f')],'fontsize',6)
%   text(0.6,0.1,['t=' num2str(res{imod+2}.mt_dec(ib)/10,'%5.2f')],'fontsize',6)
%   set(gca,'xticklabel','','yticklabel','')
% end
% 
% % export figure
% path(path,'export_fig') % remember pdftops in export_fig too
% set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 5 2])
% eval(['export_fig ' mfilename '.jpg -r600 -painters']);

save(mfilename)
