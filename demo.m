clear all; clc;

%%%%%%%%%%%%%%%%% set this variable to the model %%%%%%%%%%%%%

imod = 3; % one of 2,3,4

% 2 - model 2 - DDM with initiation after decision and changes of mind
% 3 - model 3 - DDM with preparation
% 4 - model 4 - DDM with preparation and commitment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5%%%%%%%%%%%%%%%%%%%%
% model

load generate_data_si1
clear x t

v = 0.1;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

decision = @(x) 0 + 1*(norm(x-x1)<v/2) + 2*(norm(x-x2)<v/2);

desc{1} =  '{\bf Model 1}: Action initiation after decision';
fixate{1} = @(z,b,x,y) x1*(z<b(1) && all(y~=x2) ) + y*( ~(z<b(1) && all(y~=x2) ) && ~(z>b(2) && all(y~=x1)) ) + x2*(z>b(2) && all(y~=x1));

desc{2} =  '{\bf Model 2}: Action initiation and changes of mind';
fixate{2} = @(z,b,x,y) x1*(z<b(1)) + y*(z>=b(1) && z<=b(2)) + x2*(z>b(2));

desc{3} = '{\bf Model 3}: Continual action preparation';
% fixate{3} = @(z,b,x,y) x1*(z<b(1)) + [w*(z-b(1)/2-b(2)/2)/(b(2)-b(1)) ht]*(z>=b(1) & z<=b(2)) + x2*(z>b(2));
fixate{3} = @(z,b,x,y) x1*(z<b(1)) + (x2*abs(b(1)-z)/abs(2*b(1)) + x1*abs(b(2)-z)/abs(2*b(2)))*(z>=b(1) & z<=b(2)) + x2*(z>b(2));

desc{4} = '{\bf Model 4}: Action preparation and commitment';
fixate{4} = @(z,b0,x,y) fixate{3}(z+4*b(2)*(norm(x-x1)-norm(x-x2))/(norm(x-x1)+norm(x-x2)),b,x,y);

hf = figure(1);

itr = randi(ntr);
ib = 50; b = bs(ib);
sgn = 2*(randi(2)-1.5); % -1 or 1

t = 0;
x = [0 0]; y = x(1,:);

dec = 0;
while dec==0; t = t + 1;

    y(t+1,:) = fixate{imod}(z(t,itr),[-b b],x(t,:),y(t,:));
    disp(num2str([y(t,1),z(t,itr),b]))
    
    dx = y(t+1,:) - x(t,:);
    if norm(dx)>0; dx = v*dx/norm(dx); end
    x(t+1,:) = x(t,:) + dx;
    
    dec = decision(x(t,:));
	if t==nt-1; dec = nan; end
	
	clf;
	subplot(10,2,1:4); hold on
	plot(0:t,sgn*z(1:t+1,itr),'.-r')
	plot([0 nt],b*[1 1],':k', [0 nt],-b*[1 1],':k')
	axis([0 5*ceil(t/5) [-1 1]*b*1.25])
	set(gca,'yticklabel',{'-b','-b/2','0','b/2','b'})
	set(gca,'xtick',0:5:nt,'xticklabel',(0:5:nt)*v,'fontsize',8)
	ylabel({'decision variable'},'fontsize',8)
	xlabel('time'); title(desc{imod},'fontsize',10)
	
	subplot(10,2,5:20); hold on
	plot(x1(1),x1(2),'.k', x2(1),x2(2),'.k','markersize',20)
	text(sgn*x1(1)-0.075,x1(2)+0.1,'house'); text(sgn*x2(1)-0.075,x2(2)+0.1,'houze'); 
	plot(sgn*x(1:t,1),x(1:t,2),'.-k','markersize',5)
	plot(sgn*y(t,1),y(t,2),'xr','markersize',15)
	axis([x1(1) x2(1) 0 x1(2)]+[-0.1 0.1 0 0.25])
	hold off
	
	set(gca,'visible','off'); 
	drawnow; pause(0.05)
		
end

