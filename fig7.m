clear all; clc
load generate_data_si1

v = 0.2;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

ntr = 1000; % change to 10000 for best states; to 100 for quick runs

decision = @(x) 0 + 1*(norm(x-x1)<v/2) + 2*(norm(x-x2)<v/2);

res{1}.desc = '{\bf baseline model}: DDM without action';
res{1}.mt_dec = mt_dec; res{1}.me_dec = me_dec; res{1}.nfail = sum(isnan(n_dec'));

res{2}.desc = '{\bf model 1}: action initiation after decision';
res{2}.mt_dec = mt_dec + min_rt; res{2}.me_dec = me_dec; res{2}.nfail = sum(isnan(n_dec(:)));

res{3}.desc = '{\bf model 2}: action initiation & changes of mind';
fixate{1} = @(z,b,x,y) x1*(z<b(1)) + x*(z>=b(1) && z<=b(2)) + x2*(z>b(2));

res{4}.desc = '{\bf model 3}: action preparation';
% fixate{2} = @(z,b,x,y) x1*(z<b(1)) + [w*(z-b(1)/2-b(2)/2)/(b(2)-b(1)) ht]*(z>=b(1) & z<=b(2)) + x2*(z>b(2));
fixate{2} = @(z,b,x,y) x1*(z<b(1)) + (x2*abs(b(1)-z)/abs(2*b(1)) + x1*abs(b(2)-z)/abs(2*b(2)))*(z>=b(1) & z<=b(2)) + x2*(z>b(2));

res{5}.desc = '{\bf Model 4}: Action preparation and commitment';
fixate{3} = @(z,b,x,y) fixate{2}(z+4*b(2)*(norm(x-x1)-norm(x-x2))/(norm(x-x1)+norm(x-x2)),b,x,y);

rmod = 1:3;

for imod = rmod

  for itr = 1:ntr
    if ~rem(itr,10); disp(num2str(itr)); end

    for ib = 1:nbs
      b = bs(ib);

      t = 0;
      x(1,:) = [0 0]; y(1,:) = x(1,:);

      dec = 0;
      while dec==0; t = t + 1;

        y(t+1,:) = fixate{imod}(z(t,itr),[-b b],x(t,:),y(t,:)); 

        dx = y(t+1,:) - x(t,:);
        if norm(dx)>0; dx = v*dx/norm(dx); end
        x(t+1,:) = x(t,:) + dx;

        dec = decision(x(t,:));
        if t==nt-1; dec = nan; end

      end
      
      tdec(ib,itr) = t;
      ndec(ib,itr) = dec;

    end
  end

  for ib = 1:nbs
    res{imod+2}.mt_dec(ib) = nanmean(tdec(ib,:));
    res{imod+2}.me_dec(ib) = nanmean(abs(ndec(ib,:)-tar(1:ntr)));
    res{imod+2}.nfail(ib) = sum(isnan(ndec(ib,:)));
  end

end

res{1}.desc = '{\bf Baseline Model}: DDM without action';
res{2}.desc = '{\bf Model 1}: Action initiation after decision';
res{3}.desc = '{\bf Model 2}: Action initiation & changes of mind';
res{4}.desc = '{\bf Model 3}: Action preparation';
res{5}.desc = '{\bf Model 4}: Action preparation & commitment';

figure(1); clf
subplot(1,1,1); hold on; 
col = {'--k','k','b','g','r'};
lw = [1.5 1.5 1.5 1.5 1.5];
rmods = [1 2 rmod+2]; 
for imod = rmods; plot(0,0,col{imod},'linewidth',lw(imod)); end
leg = {}; for imod = rmods; leg = {leg{:}, [res{imod}.desc]}; end
%' (fails ' int2str(sum(res{imod}.nfail)) ')'
 
for imod = rmods(end:-1:1); plot(res{imod}.mt_dec/10,100*res{imod}.me_dec,col{imod},'linewidth',lw(imod)); end
plot(min_rt/10*[1 1],100*[0 0.4],':k','linewidth',1)
ylabel('mean decision error (%)','fontsize',15)
xlabel('mean reaction time (sec)','fontsize',15)
axis([0 3 0 100*0.35])
set(gca,'ytick',100*(0:0.05:0.5),'xtick',0:0.5:3,'fontsize',13)
text(0.45,2,'T_{target}','fontsize',15)
hl = legend(leg, 'location', [0.57 0.8 0.15 0.05],'fontsize',13); set(hl,'box','off')
set(gca,'position',[0.1 0.15 0.6 0.80],'units','normalized')

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 5*6.83/5 3*6.83/5])
eval(['export_fig ' mfilename '.png -r300 -painters']);
