clear all; clc
t0 = [1.5 1.5 1.05];
rmod = 1:3;

v = 0.2;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

figure(2); clf; hold on; box on;

fill([-0.15-w/2 -0.15-w/2 w/2+0.15 w/2+0.15],[-0.15 ht+0.15 ht+0.15 -0.15],0.9*[1 1 1])
plot(-w/2,ht,'.k', w/2,ht,'.k','markersize',30)
axis([-0.15-w/2 0.15+w/2 -0.15 ht+0.15])
htit = title({'{\bf Experiment}:','Mouse-tracking data'},'fontsize',15);
pos = get(htit,'Position'); set(htit,'Position',[pos(1) pos(2)-0.0 pos(3)])
set(gca,'xticklabel','','yticklabel','')

xlabel('Horizontal position, x','fontsize',15); 
ylabel('Vertical position, y','fontsize',15);
text(-w/2+0.33,ht*1.05,{' target T1'},'horizontalalignment','center','fontsize',12); 
text(w/2-0.33,ht*1.05,{'target T2 '},'horizontalalignment','center','fontsize',12); 
text(-0.33,ht*0+0.025,{'start','location'},'horizontalalignment','center','fontsize',12); 

load('experimental_analysis','xdata','xav')
rtr = randi(150,1,10);
rtr = [  32     1    15   110    20   105    82   137    23    75];
for itr = rtr; plot(-xdata{itr}(:,2),xdata{itr}(:,1),'linewidth',1); end
plot(-xav(:,2),xav(:,1),'r','linewidth',1.5)
set(gca,'position',[0.1 0.15 0.65 0.7],'units','normalized')

% col = 'br'; leg = {'individual','trajectories','average','trajectory'};
% for i = 1:2; plot([-1.05 -0.8]+1.325,-0.675+0.1+(1-0.2*i)*[1 1],col(i)); end
% for i = 1:4; text(-0.8+0.05+1.325-0.05*~rem(i,2),-0.675+(1-0.1*i),leg{i},'fontsize',8); end

% calculate gradients
gradav1 =  mean(diff(xav(1:2,1))./diff(xav(1:2,2)))
iend = find(isnan(xav(:,1)),1,'first'); rend = (iend-200):(iend-1);
gradav2 =  mean(diff(xav(rend,1))./diff(xav(rend,2)))

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 4.5 4])
eval(['export_fig ' mfilename '.png -r600 -painters']);
