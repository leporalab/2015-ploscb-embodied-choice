clear all; clc

rmod = 1:3;

v = 0.2;     % velocity of movement
w = 2;       % width between targets
ht = 1.5;    % height of targets from starting point
min_rt = floor( sqrt( ht^2 + (w/2)^2 ) / v );

x1 = [-w/2 ht]; x2 = [w/2 ht];
t0 = [1.5 1.5 1.05];

res1{1}.desc = {'{\bf Model 1}:','Action initiation','after decision'};
res1{2}.desc = {'{\bf Model 2}:','Action initiation','& changes of mind'};
res1{3}.desc = {'{\bf Model 3}:','Action preparation',' '};
res1{4}.desc = {'{\bf Model 4}:','Action preparation','& commitment'};

figure(1); clf

for imod1 = 2:3
  subplot(1,length(rmod)-1,imod1-1); hold on; box on;
  plot(0,0,'b',0,0,'r')
  fill([-0.15-w/2 -0.15-w/2 w/2+0.15 w/2+0.15],[-0.15 ht+0.15 ht+0.15 -0.15],0.9*[1 1 1])

  load('model_si4.mat')
  [~, ib1] = min((res{imod1+2}.mt_dec/10-t0(imod1)).^2);
  plot(-w/2,ht,'.k', w/2,ht,'.k','markersize',15)
  plot([xav{ib1,imod1}(:,1)' -1],[xav{ib1,imod1}(:,2)' 1.5],'r','linewidth',0.5)

  res{imod1+2}.mt_dec(ib1)/10
  
  load('model_si1.mat')
  [~, ib1] = min((res{imod1+2}.mt_dec/10-t0(imod1)).^2);
  plot([xav{ib1,imod1}(:,1)' -1],[xav{ib1,imod1}(:,2)' 1.5],'b','linewidth',0.5)

  res{imod1+2}.mt_dec(ib1)/10
  
  axis([-0.15-w/2 0.15+w/2 -0.15 ht+0.15])
  htit = title(res1{imod1+1}.desc,'fontsize',10);
  pos = get(htit,'Position'); set(htit,'Position',[pos(1)-0.05 pos(2)-0.05 pos(3)])
  set(gca,'xticklabel','','yticklabel','')
  if imod1==2
    col = 'rb'; leg = {'4x noise','normal'};
    for i = 1:2; plot([-1 -0.6]+1,-0.2+(1+0.2*i)*[1 1],col(i)); end
    for i = 1:2; text(-0.6+0.05+1,-0.2+(1+0.2*i),leg{i},'fontsize',10); end
  end
  
  xpos = [0.15 0.50];
  set(gca,'position',[xpos(imod1-1) 0.1 0.275 0.6],'units','normalized')
  
  let = {'A','B','C','D','E'};
  text(-0.05,1.15,let{-1+imod1},'Units','Normalized','Fontsize',12,'Fontweight','bold')
  
end
 
% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 8 3.5])
eval(['export_fig ' mfilename '.png -r300 -painters']);
